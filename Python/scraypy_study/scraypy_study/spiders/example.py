# -*- coding: utf-8 -*-
import os
import sys

import scrapy

from ..items import ScraypyStudyItem
from selenium import webdriver
from .webdriver_util import Wedriver


# current_dir = os.path.abspath(os.path.dirname(__file__))
# rootPath = os.path.split(current_dir)[0]
# print(rootPath)
# sys.path.remove(rootPath)
# sys.path.append(rootPath)

class ExampleSpider(scrapy.Spider):
    name = 'book'
    allowed_domains = ['douban.com']
    start_urls = ['http://douban.com/']

    # 实例化浏览器对象  只能被调一次
    def __init__(self):
        # 静默启动
        self.browser = webdriver.Chrome(chrome_options=Wedriver.init_chrome_option())

    def parse(self, response):
        print(response)
        # print(response.text)
        # print(response.body)
        # print(response.encoding)
        item = ScraypyStudyItem()
        item["title"] = '标题: 坚持是一种力量'
        item["director"] = '导演：皮皮虾'
        return item
        # yield item

    # 在整个爬虫结束之后才会被调用
    def closed(self, spider):
        if self.browser:
            self.browser.quit()
