# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html


class ScraypyTestPipeline(object):
    # 开启爬虫时执行，只执行一次
    def open_spider(self, spider):
        # spider.hello = "world"  # 为spider对象动态添加属性，可以在spider模块中获取该属性值
        # 可以开启数据库等
        print('open_spider')

    # 处理提取的数据(保存数据)
    def process_item(self, item, spider):
        print('标题 ', item["title"])
        print('简介 ', item["director"], '\n')
        return item

    # 关闭爬虫时执行，只执行一次 (如果爬虫中间发生异常导致崩溃，close_spider可能也不会执行)
    def close_spider(self, spider):
        # 可以关闭数据库等
        print('close_spider')
