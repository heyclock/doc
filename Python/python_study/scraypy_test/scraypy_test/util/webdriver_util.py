#!/usr/bin/python3
# -*- coding: utf-8 -*-

from selenium.webdriver.chrome.options import Options
from selenium import webdriver

'''
    webdriver引擎创建管理
'''


class Wedriver:
    @staticmethod
    def get_webdriver():
        return webdriver.Chrome(chrome_options=Wedriver.init_chrome_option())

    @staticmethod
    def init_chrome_option():
        # 启动参数
        chrome_option = Options()
        prefs = {
            'profile.default_content_setting_values': {
                'images': 2,  # 禁用图片的加载
                # 'javascript': 2  # 禁用js，可能会导致通过js加载的互动数抓取失效
            }
        }
        chrome_option.add_experimental_option("prefs", prefs)
        chrome_option.add_argument('--headless')  # 使用无头谷歌浏览器模式
        chrome_option.add_argument('--disable-dev-shm-usage')
        chrome_option.add_argument('--no-sandbox')
        chrome_option.add_argument("window-size=1024,768")
        chrome_option.add_argument('--disable-gpu')
        chrome_option.add_argument('blink-settings=imagesEnabled=false')
        # https://blog.csdn.net/dichun9655/article/details/93790652 - 返爬的方式
        # chrome_option.add_experimental_option("debuggerAddress", "127.0.0.1:9222")
        return chrome_option
