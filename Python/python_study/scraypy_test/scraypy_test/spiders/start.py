# -*- coding: utf-8 -*-

from scrapy import cmdline

# 爬取id为110的页面，爬虫则会根据110字段去数据库里面取对应的链接，规则之类的信息，然后进行爬取
cmdline.execute(('scrapy crawl book -a page_id=%d %s' % (110, '--nolog')).split())
