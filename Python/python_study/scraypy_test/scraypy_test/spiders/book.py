# -*- coding: utf-8 -*-
import scrapy
from ..items import ScraypyTestItem
from ..util.webdriver_util import Wedriver


class BookSpider(scrapy.Spider):
    name = 'book'
    # allowed_domains = ['douban.com']
    start_urls = []  # 'http://douban.com/'

    # 实例化浏览器对象  只能被调一次
    def __init__(self, page_id=None):
        # 爬取id为page_id的页面的内容
        print('BookSpider __init__', page_id)
        # TODO 通过page_id获取数据库的url信息，规则信息等等
        self.b_jsload = True  # 假设是动态页面需要webdriver加载
        self.title_ruler = '//div[@class="pl2"]/a'
        self.title_brief = '//div[@class="pl2"]/p/text()'
        # 恶作剧，要被封ip的，我擦range(10000)
        for i in range(1):
            self.start_urls.append('https://movie.douban.com/chart')
        # 动态加载的情况才需要引擎支持
        if self.b_jsload:
            # 初始化webdriver引擎，用于获取动态页面内容
            self.browser = Wedriver.get_webdriver()

    # 不能写在这 因为这里也会被调多次
    def parse(self, response):
        print(response)
        # print(response.text)
        # print(response.body)
        # 然后利用xpath来解析我们需要的标题，简介等信息  xpath('string(.)')可以过滤掉内部标签，内容整合获取
        titles = response.xpath(self.title_ruler).xpath('string(.)').extract()
        briefs = response.xpath(self.title_brief).extract()
        items = []
        for index in range(len(titles)):
            # print(titles[index].replace('\n', '').replace('\r', '').replace(' ', ''))
            # print(briefs[index])
            item = ScraypyTestItem()
            item['title'] = titles[index].replace('\n', '').replace('\r', '').replace(' ', '')
            item['director'] = briefs[index]
            items.append(item)
        return items

    # 在整个爬虫结束之后才会被调用
    def closed(self, spider):
        print('BookSpider closed')
        if self.b_jsload and self.browser:
            self.browser.quit()
